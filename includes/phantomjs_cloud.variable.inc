<?php

/**
 * Implement admin forms
 *
 * @param $form
 * @param $form_state
 */

  function phantomjs_cloud_form() {

  $form = array();

  $form['title'] = array(
    '#type' => 'item',
    '#title' => t('<h2>PhantomJS Cloud admin settings</h2>'),
    );

  $form['default_settings']['description'] = array(
    '#type' => 'item',
    '#description' => t('This page contains the basic settings needed to set up phantomJS Cloud integration with your web site'),
    );

    $default_phantomjs_cloud_api_key = variable_get('phantomjs_cloud_api_key', 'a-demo-key-with-low-quota-per-ip-address');

  $form['default_settings']['phantomjs_cloud_api_key'] = array(
    '#type' => 'textfield',
    '#name' => 'phantomjs_cloud_api_key',
    '#size' => 20,
    '#title' => t('PhantomJS Cloud API key'),
    '#default_value' => $default_phantomjs_cloud_api_key,
    '#description' => t('<p>This is the key you get from PhantomJS Cloud when you register an account.</p>'),
  );

    $default_phantomjscloud_URL = variable_get('phantomjscloud_URL', 'http://api.phantomjscloud.com/single/browser/v1/');
    $form['default_settings']['phantomjscloud_URL'] = array(
      '#type' => 'textfield',
      '#name' => 'phantomjscloud_URL',
      '#size' => 20,
      '#title' => t('Path to PhantomJS Cloud'),
      '#default_value' => $default_phantomjscloud_URL,
      '#description' => t('<p>The path to PhantomJS cloud web service</p>'),
    );

    $default_load_images = variable_get('load_images', 'true');
    $form['default_settings']['load_images'] = array(
      '#type' => 'textfield',
      '#name' => 'load_images',
      '#size' => 20,
      '#title' => t('Load images?'),
      '#default_value' => $default_load_images,
      '#description' => t('<p>true or false</p>'),
    );


    $default_viewport_size = variable_get('viewport_size', '{"width":1280,"height":720,+"zoomFactor":1.0+}');
    $form['default_settings']['viewport_size'] = array(
      '#type' => 'textfield',
      '#name' => 'viewport_size',
      '#size' => 20,
      '#title' => t('Size of viewport'),
      '#default_value' => $default_viewport_size,
      '#description' => t('<p>For example: {"width":1280,"height":720,+"zoomFactor":1.0+}</p>'),
    );

    $default_image_type = variable_get('image_type', 'png');
    $form['default_settings']['image_type'] = array(
      '#type' => 'textfield',
      '#name' => 'image_type',
      '#size' => 20,
      '#title' => t('Image type'),
      '#default_value' => $default_image_type,
      '#description' => t('<p>The type of image you want to get from PhantomJS cloud</p>'),
    );

    $default_resourceUrlBlacklist = variable_get('resourceUrlBlacklist', '[]');
    $form['default_settings']['resourceUrlBlacklist'] = array(
      '#type' => 'textfield',
      '#name' => 'resourceUrlBlacklist',
      '#size' => 20,
      '#title' => t('Resource blacklist type'),
      '#default_value' => $default_resourceUrlBlacklist,
      '#description' => t('<p>The type of image you want to get from PhantomJS cloud</p>'),
    );


    $form = system_settings_form($form);
    $form['#submit'][] = 'phantomjs_cloud_form';

  return $form;
}


/**
 * Implement custom form submit
 *
 * @param $form
 * @param $form_state
 */
function phantomjs_cloud_variable_settings_form_submit($form, &$form_state)
{
  $old_value = variable_get('phantomjs_cloud_api_key', '');
  $new_value = $form_state['default_settings']['phantomjs_cloud_api_key'];
  if ($old_value != $new_value) {
    variable_set('phantomjs_cloud_api_key', $new_value);
  }

  $old_value = variable_get('phantomjscloud_URL', '');
  $new_value = $form_state['default_settings']['phantomjscloud_URL'];
  if ($old_value != $new_value) {
    variable_set('phantomjscloud_URL', $new_value);
  }

  $old_value = variable_get('load_images', '');
  $new_value = $form_state['default_settings']['load_images'];
  if ($old_value != $new_value) {
    variable_set('load_images', $new_value);
  }

  $old_value = variable_get('viewport_size', '');
  $new_value = $form_state['default_settings']['viewport_size'];
  if ($old_value != $new_value) {
    variable_set('viewport_size', $new_value);
  }

  $old_value = variable_get('phantomjs_cloud_api_key', '');
  $new_value = $form_state['default_settings']['phantomjs_cloud_api_key'];
  if ($old_value != $new_value) {
    variable_set('phantomjs_cloud_api_key', $new_value);
  }

  $old_value = variable_get('resourceUrlBlacklist', '');
  $new_value = $form_state['default_settings']['resourceUrlBlacklist'];
  if ($old_value != $new_value) {
    variable_set('resourceUrlBlacklist', $new_value);
  }
}